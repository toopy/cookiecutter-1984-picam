# target: all - Default target. Does nothing.
all:
	echo "Hello $(LOGNAME), nothing to do by default"
	# sometimes: echo "Hello ${LOGNAME}, nothing to do by default"
	echo "Try 'make help'"

# target: help - Display callable targets.
help:
	egrep "^# target:" [Mm]akefile

# target: install - Install files and enable services
install:
	# install requirements
	apt install ffmpeg libatlas-base-dev libsystemd-dev python3-pip
	pip install -U numpy picamera[array] systemd
	# copy stream script
	mkdir -p /usr/local/sbin
	cp sbin/stream /usr/local/sbin
	# setup stream service
	cp systemd/stream.env /etc/stream.env
	cp systemd/stream.service /etc/systemd/system
	systemctl daemon-reload
	systemctl enable stream
	systemctl start stream
	# setup ping service
	cp systemd/ping.* /etc/systemd/system
	systemctl daemon-reload
	systemctl enable ping.timer
	systemctl start ping.timer

# target: uninstall - Disable services and uninstall files
uninstall:
	# clean ping service
	systemctl stop ping.timer               || true
	systemctl disable ping.timer            || true
	rm /etc/systemd/system/ping.*           || true
	# clean stream service
	systemctl stop stream                   || true
	systemctl disable stream                || true
	rm /etc/systemd/system/stream.service   || true
	rm /etc/stream.env                      || true
	# clean stream script
	rm /usr/local/sbin/stream               || true

# target: start - Enable and start services
start:
	# enable and start stream service
	systemctl enable stream
	systemctl start stream
	# enable and start ping service
	systemctl enable ping.timer
	systemctl start ping.timer

# target: stop - Stop and disable services
stop:
	# stop and disable ping service
	systemctl stop ping.timer
	systemctl disable ping.timer
	# stop and disable stream service
	systemctl stop stream
	systemctl disable stream
