# CookieCutter Template For 1984-PiCam Setup

## Generate

Generate the project tree:

```bash
$ cookiecutter gl:toopy/cookiecutter-1984-picam
picam_user [pi]:
ping_timer_accuracy [0]:
ping_timer_calendar [*:0/1]:
repo_name [1984-picam]:
rtmp_api_host [api.local]:
rtmp_api_port [80]:
rtmp_api_user [foo]:
rtmp_api_pass [bar]:
rtmp_api_host [rtmp.local]:
rtmp_api_port [1935]:
stream_env_cam_framerate [12]:
stream_env_cam_resolution [854x480]:
stream_env_motion_magnetude [60]:
stream_env_motion_threshold [14]:
stream_env_stream_name [test]:
```

## Install

Install files and start services

```bash
$ sudo make -C 1984-picam install
# install requirements
...
systemctl start ping.timer
```

## LICENSE

MIT
